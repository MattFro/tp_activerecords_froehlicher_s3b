package testEtu;

import activeRecord.DBConnection;
import org.junit.Assert;

import java.sql.Connection;

import static org.junit.Assert.*;

public class DBConnectionTest {

    @org.junit.Test
    public void getConnection() {
        DBConnection cd = DBConnection.getInstance();
        cd.setDbName("db");
        Assert.assertEquals("le nom devrait etre db","db", cd.getDbName());
    }
}