package testEtu;

import activeRecord.Personne;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;

public class PersonneTest {

    @Before
    public void setUp() throws Exception {
        Personne.createTable();
        (new Personne("Spielberg", "Steven")).save();
        (new Personne("Scott", "Ridley")).save();
        (new Personne("Kubrick", "Stanley")).save();
        (new Personne("Spielberg", "George")).save();
    }

    @After
    public void tearDown() throws Exception {
        Personne.deleteTable();
    }

    @Test
    public void testFindByNameFaux() throws SQLException {
        ArrayList<Personne> p = Personne.findByName("t");
        Assert.assertEquals("Le nom n'existe pas", 0, p.size());
    }

    @Test
    public void testFindByName() throws SQLException {
        ArrayList<Personne> p = Personne.findByName("Spielberg");
        Personne pers = (Personne)p.get(0);
        Assert.assertEquals(pers.getNom(), "Spielberg");
        Assert.assertEquals(pers.getPrenom(), "Steven");
    }


    @Test
    public void testDelete() throws SQLException {
        Personne p = Personne.findById(4);
        p.delete();
        Personne p2 = Personne.findById(4);
        Assert.assertEquals("la personne n'existe plus", (Object)null, p2);
    }

    @Test
    public void testSaveNouveau() throws SQLException {
        Personne p = new Personne("Lucas", "George");
        p.save();
        Assert.assertEquals("id devrait etre incrementer", 5, p.getId());
        Personne pers = Personne.findById(5);
        Assert.assertEquals(pers.getNom(), "Lucas");
        Assert.assertEquals(pers.getPrenom(), "George");
    }





}