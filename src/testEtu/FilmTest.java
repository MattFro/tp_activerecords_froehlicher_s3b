package testEtu;

import activeRecord.Film;
import activeRecord.Personne;
import activeRecord.RealisateurAbsentException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;

import static org.junit.Assert.*;

public class FilmTest {

    Personne spielberg;
    Personne scott;
    Personne kubrick;
    Personne fincher;

    @Before
    public void setUp() throws SQLException, RealisateurAbsentException {
        Personne.createTable();
        Film.createTable();
        this.spielberg = new Personne("Spielberg", "Steven");
        this.spielberg.save();
        this.scott = new Personne("Scott", "Ridley");
        this.scott.save();
        this.kubrick = new Personne("Kubrick", "Stanley");
        this.kubrick.save();
        this.fincher = new Personne("Fincher", "David");
        this.fincher.save();
        (new Film("Arche perdue", this.spielberg)).save();
        (new Film("Alien", this.scott)).save();
        (new Film("Temple Maudit", this.spielberg)).save();
        (new Film("Blade Runner", this.scott)).save();
        (new Film("Alien3", this.fincher)).save();
        (new Film("Fight Club", this.fincher)).save();
        (new Film("Orange Mecanique", this.kubrick)).save();
    }

    @After
    public void tearDown() throws SQLException {
        Film.deleteTable();
        Personne.deleteTable();
    }

    @Test
    public void testfindByIdFaux() throws SQLException {
        Film f = Film.findById(12);
        Assert.assertEquals("le film n'existe pas",null,f);
    }

    @Test
    public void testfindById() throws SQLException {
        Film f = Film.findById(3);
        Assert.assertEquals("Temple Maudit", f.getTitre());
        Assert.assertEquals("Spielberg", f.getRealisateur().getNom());
    }



    @Test
    public void testGetRealisateur() throws SQLException, RealisateurAbsentException {
        Film f = new Film("Alien3",this.fincher);
        Assert.assertEquals(4, (long)f.getRealisateur().getId());
    }


    @Test
    public void testSave() throws SQLException, RealisateurAbsentException {
        Film f = new Film("ET", this.spielberg);
        f.save();
        Film f2 = Film.findById(8);
        Assert.assertEquals("ET", f2.getTitre());
        Assert.assertEquals("Spielberg", f2.getRealisateur().getNom());
        Assert.assertEquals(1, (long)f2.getRealisateur().getId());
    }


}