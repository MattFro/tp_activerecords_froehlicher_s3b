package activeRecord;

import java.sql.*;
import java.util.ArrayList;

public class Film {

    private String titre;
    private int id, id_real;

     public Film(String t, Personne p){
         this.titre=t;
         this.id_real=p.getId();
         this.id=-1;
     }

     private Film(int id, String t, int idr){
         this.titre=t;
         this.id=id;
         this.id_real=idr;
     }

    public static Film findById(int i) {
        Film f = null;
        try {
            DBConnection connect = DBConnection.getInstance();
            Connection c = connect.getConnection();
            String SQLPrep = "SELECT * FROM Film WHERE id = ?";
            PreparedStatement prep1 = c.prepareStatement(SQLPrep);
            prep1.setInt(1, i);
            ResultSet rs = prep1.executeQuery();
            if (rs.next()) {
                int idr = rs.getInt("id_rea");
                String titre=rs.getString("titre");
               int id= rs.getInt("id");
                Film t= new Film(id,titre,idr);
                t.id=id;
                f=t;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return f;
    }

    public Personne getRealisateur(){
        return Personne.findById(this.id_real);
    }


    public static void createTable() throws SQLException {
        DBConnection connect = DBConnection.getInstance();
        Connection c = connect.getConnection();
        String SQLprep = "CREATE TABLE IF NOT EXISTS `film` (\n" +
                "  `ID` int(11) NOT NULL AUTO_INCREMENT,\n" +
                "  `TITRE` varchar(40) NOT NULL,\n" +
                "  `ID_REA` int(11) DEFAULT NULL,\n" +
                "  PRIMARY KEY (`ID`),\n" +
                "  KEY `ID_REA` (`ID_REA`)\n" +
                ") ENGINE=InnoDB  DEFAULT CHARSET=latin1";
        PreparedStatement prep1 = c.prepareStatement(SQLprep);
        prep1.execute();
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setId_real(int id_real) {
        this.id_real = id_real;
    }

    public static void deleteTable() throws SQLException {
        DBConnection connect = DBConnection.getInstance();
        Connection c = connect.getConnection();
        String SQLprep = "drop table Film";
        PreparedStatement prep1 = c.prepareStatement(SQLprep);
        prep1.execute();
    }

    public void delete() throws SQLException {
        DBConnection connect = DBConnection.getInstance();
        Connection c = connect.getConnection();
        String SQLprep = "delete from Film where ID=?";
        PreparedStatement prep1 = c.prepareStatement(SQLprep);
        prep1.setInt(1,this.id);
        prep1.execute();
        this.id=-1;
    }

    public void save() throws SQLException, RealisateurAbsentException {
        if (this.id_real<0){
            throw new RealisateurAbsentException("le réalisateur n'existe pas");
        }if(this.id==-1) {
            saveNew(this);
        }else{
            update(this);
        }
    }


    private static void saveNew(Film t) throws SQLException {
        DBConnection connect = DBConnection.getInstance();
        Connection c = connect.getConnection();
        String SQLprep = "insert into Film (titre,ID_REA) values (?,?)";
        PreparedStatement prep1 = c.prepareStatement(SQLprep, Statement.RETURN_GENERATED_KEYS);
        prep1.setString(1, t.titre);
        prep1.setInt(2, t.id_real);
        prep1.executeUpdate();
        ResultSet rs = prep1.getGeneratedKeys();
        rs.next();
        t.id=rs.getInt(1);
    }


    private static void update(Film f) throws SQLException {
        DBConnection connect = DBConnection.getInstance();
        Connection c = connect.getConnection();
        String SQLprep = "update Film set titre = ?, id_rea = ? where id = ?";
        PreparedStatement prep1 = c.prepareStatement(SQLprep);
        prep1.setInt(3,f.id);
        prep1.setString(1, f.titre);
        prep1.setInt(2, f.id_real);
        prep1.executeUpdate();
    }


    public String getTitre() {
        return titre;
    }

    public int getId() {
        return id;
    }

    public int getId_real() {
        return id_real;
    }

    public ArrayList<Film> findByRealisateur(Personne p) throws SQLException {
         ArrayList<Film> f = new ArrayList<Film>();
        DBConnection connect = DBConnection.getInstance();
        Connection c = connect.getConnection();
         String SQLprep = "select * from Film where id_real=?";
         PreparedStatement prep1 = c.prepareStatement(SQLprep);
         prep1.setInt(1,p.getId());
         prep1.execute();
        ResultSet rs = prep1.getResultSet();
        if (rs.next()) {
            String titre = rs.getString("titre");
            int idr= rs.getInt("id_real");
            int i = rs.getInt("id");
            Film a = new Film(i,titre,idr);
            f.add(a);
        }

        return f;

    }
}
