package activeRecord;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

    private String userName;
    private String password;
    private String serverName;
    private String portNumber;
    private String tableName;
    private String dbName;
    private Connection c;
    private static DBConnection instance;

    private DBConnection(){
        userName = "root";
        password = "";
        serverName = "localhost";
        portNumber = "3306";
        tableName = "personne";
        dbName = "testpersonne";
        try{
            String url = "jdbc:mysql://127.0.0.1:3306/testpersonne";
            c= DriverManager.getConnection(url,userName, password);
        }catch(SQLException e){
            System.out.println(e);
        }
    }


    public synchronized static DBConnection getInstance(){
        if(instance == null){
            instance = new DBConnection();
        }else {
            try {
                if(instance.c!=null && instance.c.isClosed()){
                    instance = new DBConnection();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
        instance = DBConnection.getInstance();
    }


    public Connection getConnection() {
        return c;
    }

    public String getDbName() {
        return dbName;
    }
}
