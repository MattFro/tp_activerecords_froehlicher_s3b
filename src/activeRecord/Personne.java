package activeRecord;

import javax.swing.plaf.nimbus.State;
import java.sql.*;
import java.util.ArrayList;

public class Personne {

    private int id;
    private String nom, prenom;


    public Personne(String n, String p) {
        this.nom = n;
        this.prenom = p;
        this.id = -1;
    }


    public static ArrayList<Personne> findAll() {
        ArrayList<Personne> res = new ArrayList<Personne>();
        try {
            DBConnection connect = DBConnection.getInstance();
            Connection c = connect.getConnection();
            String SQLprep = "select * from Personne";
            PreparedStatement prep1 = c.prepareStatement(SQLprep);
            prep1.execute();

            ResultSet rs = prep1.getResultSet();
            while (rs.next()) {
                String nom = rs.getString("nom");
                String prenom = rs.getString("prenom");
                int id = rs.getInt("id");
                Personne p = new Personne(nom, prenom);
                p.id = id;
                res.add(p);
            }
        } catch (SQLException e) {
            System.out.println("Exception SQL");
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println("Exception Inconnue");
            e.printStackTrace();
        }
        return res;
    }


    public static Personne findById(int id) {
        Personne p = null;
        try {
            DBConnection connect = DBConnection.getInstance();
            Connection c = connect.getConnection();
            String SQLPrep = "select * from Personne where id = ?";
            PreparedStatement prep1 = c.prepareStatement(SQLPrep);
            prep1.setInt(1, id);
            ResultSet rs = prep1.executeQuery();
            if (rs.next()) {
                String nom = rs.getString("nom");
                String prenom = rs.getString("prenom");
                int i = rs.getInt("id");
                p = new Personne(nom, prenom);
                p.setId(i);
                System.out.println(p.getNom());
                System.out.println(p.getPrenom());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return p;
    }

    public static ArrayList<Personne> findByName(String nom) {
        ArrayList<Personne> res = new ArrayList<Personne>();
        try {
            DBConnection connect = DBConnection.getInstance();
            Connection c = connect.getConnection();
            String SQLprep = "select * from Personne where nom=?";
            PreparedStatement prep1 = c.prepareStatement(SQLprep);
            prep1.setString(1, nom);
            prep1.execute();

            ResultSet rs = prep1.getResultSet();
            while (rs.next()) {
                String n = rs.getString("nom");
                String prenom = rs.getString("prenom");
                int id = rs.getInt("id");
                Personne p = new Personne(n, prenom);
                p.setId(id);
                res.add(p);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    return res;
    }

    public static void createTable() throws SQLException {
        DBConnection connect = DBConnection.getInstance();
        Connection c = connect.getConnection();
        String SQLprep = "CREATE TABLE IF NOT EXISTS `personne` (`ID` int(11) NOT NULL AUTO_INCREMENT,`NOM` varchar(40) NOT NULL, `PRENOM` varchar(40) NOT NULL,PRIMARY KEY (`ID`)) ENGINE=InnoDB  DEFAULT CHARSET=latin1";
        PreparedStatement prep1 = c.prepareStatement(SQLprep);
        prep1.execute();
    }

    public static void deleteTable() throws SQLException {
        DBConnection connect = DBConnection.getInstance();
        Connection c = connect.getConnection();
        String SQLprep = "drop table Personne";
        PreparedStatement prep1 = c.prepareStatement(SQLprep);
        prep1.execute();
    }


    public void delete() throws SQLException {
        DBConnection connect = DBConnection.getInstance();
        Connection c = connect.getConnection();
        String SQLprep = "delete from Personne where ID=?";
        PreparedStatement prep1 = c.prepareStatement(SQLprep);
        prep1.setInt(1,this.id);
        prep1.execute();
        this.id=-1;
    }

    public void save() throws SQLException {
        if (this.id==-1) {
            this.saveNew(this);
        }else
            this.update(this);
        }


    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    private void saveNew(Personne p) throws SQLException {
        DBConnection connect = DBConnection.getInstance();
        Connection c = connect.getConnection();
        String SQLprep = "insert into personne (nom,prenom) values (?,?)";
        PreparedStatement prep1 = c.prepareStatement(SQLprep,Statement.RETURN_GENERATED_KEYS);
        prep1.setString(1, p.nom);
        prep1.setString(2, p.prenom);
        prep1.executeUpdate();
        ResultSet rs = prep1.getGeneratedKeys();
        rs.next();
        p.id=rs.getInt(1);


    }


    public int getId() {
        return id;
    }

    private void update(Personne p) throws SQLException {
        DBConnection connect = DBConnection.getInstance();
        Connection c = connect.getConnection();
        String SQLprep = "update personne set nom = ?, prenom = ? where id=?";
        PreparedStatement prep1 = c.prepareStatement(SQLprep);
        prep1.setString(1, p.nom);
        prep1.setString(2, p.prenom);
        prep1.setInt(3,p.id);
        prep1.executeUpdate();
    }


}

